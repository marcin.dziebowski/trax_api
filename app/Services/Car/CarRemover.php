<?php

namespace App\Services\Car;

use App\Car;
use Illuminate\Support\Collection;

/**
 * Class CarRemover
 * @package App\Services\Car
 */
class CarRemover implements AbleToRemoveCar
{
    /**
     * @inheritDoc
     */
    public function remove(int $id): Collection
    {
        $car = Car::singleForAuthUser($id)->first();
        $car->delete();

        return Car::allForAuthUser()->get();
    }
}
