<?php

namespace App\Services\Car;

use App\Car;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AbleToProvideCar
 * @package App\Services\Car
 */
interface AbleToProvideCar
{
    /**
     * @return Collection
     */
    public function provideAll(): Collection;

    /**
     * @param int $id
     * @return Car
     */
    public function provide(int $id): Car;
}
