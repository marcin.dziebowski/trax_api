<?php

namespace App\Services\Car;

use App\Car;

/**
 * Class AbleToCreateCar
 * @package App\Services\Car
 */
interface AbleToCreateCar
{
    /**
     * @param array $data
     * @return Car
     */
    public function create(array $data): Car;
}
