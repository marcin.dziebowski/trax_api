<?php

namespace App\Services\Car;

use App\Car;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CarProvider
 * @package App\Services\Car
 */
class CarProvider implements AbleToProvideCar
{
    /**
     * @inheritDoc
     */
    public function provideAll(): Collection
    {
        return Car::allForAuthUser()->get();
    }

    /**
     * @inheritDoc
     */
    public function provide(int $id): Car
    {
        return Car::singleForAuthUser($id)->first();
    }
}
