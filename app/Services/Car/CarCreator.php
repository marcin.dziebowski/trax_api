<?php

namespace App\Services\Car;

use App\Car;

/**
 * Class CarCreator
 * @package App\Services\Car
 */
class CarCreator implements AbleToCreateCar
{
    /**
     * @inheritDoc
     */
    public function create(array $data): Car
    {
        //assign current logged user
        $data['user_id'] = auth('api')->user()->id;

        $newCar = new Car($data);
        $newCar->save();;

        return $newCar;
    }
}
