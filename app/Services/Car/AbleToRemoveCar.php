<?php

namespace App\Services\Car;

use Illuminate\Support\Collection;

/**
 * Class AbleToRemoveCar
 * @package App\Services\Car
 */
interface AbleToRemoveCar
{
    /**
     * @param int $id
     * @return Collection
     */
    public function remove(int $id): Collection;
}
