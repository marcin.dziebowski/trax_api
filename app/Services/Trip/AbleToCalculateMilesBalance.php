<?php

namespace App\Services\Trip;

/**
 * Class AbleToCalculateMilesBalance
 * @package App\Services\Trip
 */
interface AbleToCalculateMilesBalance
{
    /**
     * @param array $data
     */
    public function calculate(array &$data) : void;
}
