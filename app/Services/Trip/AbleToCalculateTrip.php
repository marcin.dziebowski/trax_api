<?php

namespace App\Services\Trip;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AbleToCalculateTrip
 * @package App\Services\Trip
 */
interface AbleToCalculateTrip
{
    /**
     * @param JsonResource $model
     * @return float
     */
    public function calculate(JsonResource $model): float;
}
