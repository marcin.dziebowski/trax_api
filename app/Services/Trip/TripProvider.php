<?php

namespace App\Services\Trip;

use App\Trip;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class TripProvider
 * @package App\Services\Trip
 */
class TripProvider implements AbleToProvideTrip
{
    /**
     * @inheritDoc
     */
    public function provideAll(): Collection
    {
        return Trip::allForAuthUser()->get();
    }

    /**
     * @inheritDoc
     */
    public function provide(int $id): Trip
    {
        return Trip::singleForAuthUser($id)->first();
    }
}
