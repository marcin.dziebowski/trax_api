<?php

namespace App\Services\Trip;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TripCalculator
 * @package App\Services\Trip
 */
class TripCalculator implements AbleToCalculateTrip
{
    /**
     * @inheritDoc
     */
    public function calculate(JsonResource $model): float
    {
        $countOfMiles = 0.0;
        foreach ($model->trips as $trip) {
            $countOfMiles += $trip->miles;
        }

        return $countOfMiles;
    }
}
