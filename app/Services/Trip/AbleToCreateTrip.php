<?php

namespace App\Services\Trip;

use App\Trip;

/**
 * Class AbleToCreateTrip
 * @package App\Services\Trip
 */
interface AbleToCreateTrip
{
    /**
     * @param array $data
     * @return Trip
     */
    public function create(array $data): Trip;
}
