<?php

namespace App\Services\Trip;

use App\Trip;

/**
 * Class TripCreator
 * @package App\Services\Trip
 */
class TripCreator implements AbleToCreateTrip
{
    /**
     * @var AbleToCalculateMilesBalance
     */
    private $milesBalanceCalculator;

    /**
     * @param TripMilesBalanceCalculator $milesBalanceCalculator
     */
    public function __construct(TripMilesBalanceCalculator $milesBalanceCalculator)
    {
        $this->milesBalanceCalculator = $milesBalanceCalculator;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Trip
    {
        //assign current logged user
        $data['user_id'] = auth('api')->user()->id;
        $this->milesBalanceCalculator->calculate($data);

        $newTrip = new Trip($data);
        $newTrip->save();;

        return $newTrip;
    }
}
