<?php

namespace App\Services\Trip;

use App\Trip;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class AbleToProvideTrip
 * @package App\Services\Trip
 */
interface AbleToProvideTrip
{
    /**
     * @return Collection
     */
    public function provideAll(): Collection;

    /**
     * @param int $id
     * @return Trip
     */
    public function provide(int $id): Trip;
}
