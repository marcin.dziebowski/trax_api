<?php

namespace App\Services\Trip;

use App\Trip;

/**
 * Class TripMilesBalanceCalculator
 * @package App\Services\Trip
 */
class TripMilesBalanceCalculator implements AbleToCalculateMilesBalance
{
    /**
     * @inheritDoc
     */
    public function calculate(array &$data) : void
    {
        $milesBalance = Trip::countOfAllTripsMiles(intval($data['car_id']))->sum('miles');
        $milesBalance += $data['miles'];

        $data['miles_balance'] = $milesBalance;
    }
}
