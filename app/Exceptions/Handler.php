<?php

namespace App\Exceptions;

use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @var CustomApiExceptionHandler
     */
    public $customApiExceptionHandler;

    /**
     * @param \Throwable $exception
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * @param Container $container
     * @param CustomApiExceptionHandler $customApiExceptionHandler
     */
    public function __construct(Container $container, CustomApiExceptionHandler $customApiExceptionHandler)
    {
        parent::__construct($container);
        $this->customApiExceptionHandler = $customApiExceptionHandler;
    }

    /**
     * @param Request $request
     * @param \Throwable $exception
     *
     * @return Response
     * @throws \Throwable
     */
    public function render($request, Throwable $exception): Response
    {
        if ($request->wantsJson()) {   //add Accept: application/json in request
            return $this->customApiExceptionHandler->handleApiException($request, $exception);
        } else {
            $retval = parent::render($request, $exception);
        }

        return $retval;
    }
}
