<?php

namespace App\Policies;

use App\Car;
use App\User;

/**
 * Class CarPolicy
 * @package App\Policies
 */
class CarPolicy
{
    /**
     * Determine if the given Car can be view by the user.
     *
     * @param User $user
     * @param Car $car
     * @return bool
     */
    public function view(User $user, Car $car): bool
    {
        return $user->id === $car->user_id;
    }

    /**
     * Determine if the given Car can be delete by the user.
     *
     * @param User $user
     * @param Car $car
     * @return bool
     */
    public function delete(User $user, Car $car): bool
    {
        return $user->id === $car->user_id;
    }

    //Store we can add if we have eg. roles and permission
    //public function store():

    //Store we can add if we have eg. roles and permission
    //public function viewAny():

    //This same situation if we will create TripPolicy
}
