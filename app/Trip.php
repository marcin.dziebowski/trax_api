<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Trip
 * @package App
 *
 * @property int $id
 * @property string $date
 * @property float $miles
 * @property float $miles_balance
 * @property int $car_id
 * @property int $user_id
 */
class Trip extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $casts = [
        'date' => 'date:d/m/Y',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date', 'miles', 'car_id', 'user_id', 'miles_balance'];

    /**
     * Get the Car that owns the Trip
     *
     * @return BelongsTo
     */
    public function car(): BelongsTo
    {
        return $this->belongsTo(Car::class);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeAllForAuthUser(Builder $query): Builder
    {
        return $query->where('user_id', auth('api')->user()->id)->with('car');
    }

    /**
     * @param Builder $query
     * @param int $id
     * @return Builder
     */
    public function scopeSingleForAuthUser(Builder $query, int $id): Builder
    {
        return $query->where([
            ['id', $id],
            ['user_id', auth('api')->user()->id]
        ])->with('car');
    }

    /**
     * @param Builder $query
     * @param int $carId
     * @return Builder
     */
    public function scopeCountOfAllTripsMiles(Builder $query, int $carId): Builder
    {
        return $query->where([
            ['car_id', $carId],
            ['user_id', auth('api')->user()->id]
        ]);
    }
}
