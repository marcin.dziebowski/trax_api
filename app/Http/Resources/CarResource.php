<?php

namespace App\Http\Resources;

use App\Services\Trip\TripCalculator;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CarRequest
 * @package App\Http\Resources
 */
class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'make' => $this->make,
            'model' => $this->model,
            'year' => $this->year,
            'user' => $this->user,
            'trips' => $this->trips,
            'trip_count' => $this->trips->count(),
            'trip_miles' => (new TripCalculator())->calculate($this),
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
        ];
    }
}
