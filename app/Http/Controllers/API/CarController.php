<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarRequest;
use App\Http\Resources\CarResource;
use App\Services\Car\CarCreator;
use App\Services\Car\CarProvider;
use App\Services\Car\CarRemover;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class CarController
 * @package App\Http\Controllers
 */
class CarController extends Controller
{
    /**
     * Show all Cars
     * @param CarProvider $carProvider
     * @return AnonymousResourceCollection
     */
    public function index(CarProvider $carProvider): AnonymousResourceCollection
    {
        return CarResource::collection($carProvider->provideAll());
    }

    /**
     * Create new Car
     * @param CarRequest $request
     * @param CarCreator $carCreator
     * @return CarResource
     */
    public function store(CarRequest $request, CarCreator $carCreator): CarResource
    {
        return new CarResource($carCreator->create($request->all()));
    }

    /**
     * Show Car with a given id
     * @param int $id
     * @param CarProvider $carProvider
     * @return CarResource
     * @throws AuthorizationException
     */
    public function show(
        int $id,
        CarProvider $carProvider
    ): CarResource {
        $car = $carProvider->provide($id);
        $this->authorize('view', $car);

        return new CarResource($car);
    }

    /**
     * Delete Car with a given id
     * @param int $id
     * @param CarProvider $carProvider
     * @param CarRemover $carRemover
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(
        int $id,
        CarProvider $carProvider,
        CarRemover $carRemover
    ): JsonResponse {
        $this->authorize('delete', $carProvider->provide($id));

        return response()->json($carRemover->remove($id));
    }
}
