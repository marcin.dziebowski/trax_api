<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\TripRequest;
use App\Http\Resources\TripResource;
use App\Services\Trip\TripCreator;
use App\Services\Trip\TripProvider;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

/**
 * Class TripController
 * @package App\Http\Controllers
 */
class TripController extends Controller
{
    /**
     * Show all Trips
     * @param TripProvider $tripProvider
     * @return AnonymousResourceCollection
     */
    public function index(
        TripProvider $tripProvider
    ): AnonymousResourceCollection {
        return TripResource::collection($tripProvider->provideAll(), 200);
    }

    /**
     * Create new Trip
     * @param TripRequest $request
     * @param TripCreator $tripCreator
     * @return TripResource
     */
    public function store(TripRequest $request, TripCreator $tripCreator): TripResource
    {
        return new TripResource($tripCreator->create($request->all()));
    }
}
