<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Car
 * @package App
 *
 * @property int $id
 * @property string $make
 * @property string $model
 * @property int $year
 * @property int $user_id
 * @property int trip_count
 * @property float trip_miles
 */
class Car extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['make', 'model', 'year', 'user_id'];

    /**
     * Get the Tips for Car
     * @return HasMany
     */
    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    /**
     * Get the User that owns the Car
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeAllForAuthUser(Builder $query): Builder
    {
        return $query->where('user_id', auth('api')->user()->id);
    }

    /**
     * @param Builder $query
     * @param int $id
     * @return Builder
     */
    public function scopeSingleForAuthUser(Builder $query, int $id): Builder
    {
        return $query->where([
            ['id', $id],
            ['user_id', auth('api')->user()->id]
        ])->with('trips');
    }
}
