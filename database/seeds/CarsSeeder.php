<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class CarsSeeder
 */
class CarsSeeder extends Seeder
{
    /** @var array[] */
    private $cars = array(
        array('make' => 'Land Rover', 'model' => 'Range Rover Sport', 'year' => 2017),
        array('make' => 'Ford', 'model' => 'F150', 'year' => 2014),
        array('make' => 'Chevy', 'model' => 'Tahoe', 'year' => 2015),
        array('make' => 'Aston Martin', 'model' => 'Vanquish', 'year' => 2018),
    );

    /**
     * Seed the application's database.
     *
     * @param int $userId
     * @return void
     */
    public function run(int $userId)
    {
        foreach ($this->cars as $car) {
            DB::table('cars')->updateOrInsert([
                'make' => $car['make'],
                'model' => $car['model'],
                'year' => $car['year'],
                'user_id' => $userId
            ]);
        }
    }
}
