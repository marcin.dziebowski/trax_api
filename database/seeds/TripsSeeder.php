<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class TripsSeeder
 */
class TripsSeeder extends Seeder
{
    /** @var array[]  */
    private $trips = array(
        array('car_id' => 1),
        array('car_id' => 2),
        array('car_id' => 3),
        array('car_id' => 4),
    );

    /**
     * Seed the application's database.
     *
     * @param int $userId
     * @return void
     */
    public function run(int $userId)
    {
        $subDays = 1;
        foreach ($this->trips as $trip) {
            DB::table('trips')->updateOrInsert([
                'date' => Carbon::now()->subDays($subDays)->format('m/d/Y'),
                'miles' => pow(rand(1, 100), 1),
                'car_id' => $trip['car_id'],
                'user_id' => $userId
            ]);
            $subDays++;
        }
    }
}

