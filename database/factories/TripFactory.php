<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Trip::class, function (Faker $faker) {
    return [
        'date' => '06/10/2022',
        'miles' => 10.00,
        'car_id' => 1,
        'user_id' => 2,
    ];
});
