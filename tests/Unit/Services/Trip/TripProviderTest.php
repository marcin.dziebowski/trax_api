<?php

namespace Services\Trip;

use App\Services\Trip\AbleToProvideTrip;
use App\Trip;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

/**
 * Class TripProviderTest
 */
class TripProviderTest extends TestCase
{
    public function testProvideAll()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $mock = \Mockery::mock(AbleToProvideTrip::class);
        $mock->shouldReceive('provideAll')->once()->andReturn(new Collection());

        $mock->provideAll();
    }

    public function testProvide()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $mock = \Mockery::mock(AbleToProvideTrip::class);
        $mock->shouldReceive('provide')->with(1)->once()->andReturn(new Trip());

        $mock->provide(1);
    }
}
