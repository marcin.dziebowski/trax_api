<?php

namespace Services\Trip;

use App\Services\Trip\AbleToCreateTrip;
use App\Trip;
use App\User;
use Tests\TestCase;

/**
 * Class TripCreatorTest
 */
class TripCreatorTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testCreate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $trip = factory(Trip::class)->create();

        $mock = \Mockery::mock(AbleToCreateTrip::class);
        $mock->shouldReceive('create')->with($trip->toArray())->once()->andReturn(new Trip());

        $mock->create($trip->toArray());

        $trip->delete();
    }
}
