<?php

namespace Services\Trip;

use App\Car;
use App\Http\Resources\CarResource;
use App\Services\Trip\AbleToCalculateTrip;
use App\User;
use Tests\TestCase;

/**
 * Class TripCalculatorTest
 */
class TripCalculatorTest extends TestCase
{
    public function testCalculate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->make();

        $mock = \Mockery::mock(AbleToCalculateTrip::class);
        $mock->shouldReceive('calculate')->once();

        $resource = new CarResource($car);

        $mock->calculate($resource);
    }
}
