<?php

namespace Services\Trip;

use App\Car;
use App\Services\Trip\AbleToCalculateMilesBalance;
use App\Services\Trip\TripMilesBalanceCalculator;
use App\Trip;
use App\User;
use Tests\TestCase;

/**
 * Class TripCreatorTest
 */
class TripMilesBalanceCalculatorTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testCalculate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');
        $car = factory(Car::class)->create();

        $data['miles'] = 10;
        $data['car_id'] = $car->id;

        $tripMilesBalanceCalculator = new TripMilesBalanceCalculator();
        $tripMilesBalanceCalculator->calculate($data);

        $this->assertArrayHasKey('miles_balance', $data);
        $this->assertIsArray($data);

        $car->delete();
    }
}
