<?php

namespace Services\Car;

use App\Services\Car\AbleToRemoveCar;
use App\Services\Car\CarRemover;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

/**
 * Class CarRemoverTest
 */
class CarRemoverTest extends TestCase
{
    public function testRemove()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $mock = \Mockery::mock(AbleToRemoveCar::class);
        $mock->shouldReceive('remove')->with(1)->once()->andReturn(new Collection());

        $mock->remove(1);
    }
}
