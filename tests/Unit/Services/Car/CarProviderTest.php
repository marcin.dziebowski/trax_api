<?php

namespace Services\Car;

use App\Car;
use App\Services\Car\AbleToProvideCar;
use App\Services\Car\CarProvider;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

/**
 * Class CarProviderTest
 */
class CarProviderTest extends TestCase
{
    public function testProvideAll()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $mock = \Mockery::mock(AbleToProvideCar::class);
        $mock->shouldReceive('provideAll')->once()->andReturn(new Collection());

        $mock->provideAll();
    }

    public function testProvide()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $mock = \Mockery::mock(AbleToProvideCar::class);
        $mock->shouldReceive('provide')->with(1)->once()->andReturn(new Car());

        $mock->provide(1);
    }
}
