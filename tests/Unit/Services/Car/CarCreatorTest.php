<?php

namespace Services\Car;

use App\Car;
use App\Services\Car\AbleToCreateCar;
use App\Services\Car\CarCreator;
use App\User;
use Tests\TestCase;

/**
 * Class CarCreatorTest
 */
class CarCreatorTest extends TestCase
{
    /**
     * Test if Car wil be created
     * @throws \Exception
     */
    public function testCreate()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->create();

        $mock = \Mockery::mock(AbleToCreateCar::class);
        $mock->shouldReceive('create')->with($car->toArray())->once()->andReturn(new Car());

        $mock->create($car->toArray());

        $car->delete();
    }
}
