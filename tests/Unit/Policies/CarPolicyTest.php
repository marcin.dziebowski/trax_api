<?php

namespace Policies;

use App\Car;
use App\User;
use Tests\TestCase;

/**
 * Class CarPolicyTest
 */
class CarPolicyTest extends TestCase
{
    /**
     * Test if auth user can see own car
     * @throws \Exception
     */
    public function testUserCanSeeCar()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api');
        $response = $this->call('GET', route('api_cars_show', $car->id));
        $response->assertStatus(200);

        $car->delete();
    }

    /**
     * Test if auth user can delete own car
     * @throws \Exception
     */
    public function testUserCanDeleteCar()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->create(['user_id' => $user->id]);

        $this->actingAs($user, 'api');
        $response = $this->call('DELETE', route('api_cars_delete', $car->id));
        $response->assertStatus(200);

        $car->delete();
    }
}
