<?php

namespace Http\Requests;

use App\Http\Requests\CarRequest;
use Tests\TestCase;

/**
 * Class CarRequestTest
 */
class CarRequestTest extends TestCase
{
    /** @var string[] */
    protected $rules;

    /**
     * Set up operations
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->rules = (new CarRequest())->rules();
        $this->validator = $this->app['validator'];
    }

    /**
     * Test if year field is valid
     */
    public function testValidYear() : void
    {
        $this->assertTrue($this->validateField('year', 2019));
        $this->assertFalse($this->validateField('year', null));
    }

    /**
     * Test if make field is valid
     */
    public function testValidMake() : void
    {
        $this->assertTrue($this->validateField('make', 'Ford'));
        $this->assertFalse($this->validateField('make', null));
    }

    /**
     * Test if model field is valid
     */
    public function testValidModel() : void
    {
        $this->assertTrue($this->validateField('model', 'Mustang'));
        $this->assertFalse($this->validateField('model', null));
    }

    /**
     * Check a field and value against validation rule
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    protected function validateField(string $field, $value): bool
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        )->passes();
    }
}
