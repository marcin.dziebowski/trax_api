<?php

namespace Http\Requests;

use App\Http\Requests\TripRequest;
use Tests\TestCase;

/**
 * Class TripRequestTest
 */
class TripRequestTest extends TestCase
{
    /** @var string[] */
    protected $rules;

    /**
     * Set up operations
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->rules = (new TripRequest())->rules();
        $this->validator = $this->app['validator'];
    }

    /**
     * Test if date field is valid
     */
    public function testValidDate(): void
    {
        $this->assertTrue($this->validateField('date', '06/10/2022'));
        $this->assertFalse($this->validateField('date', null));
    }

    /**
     * Test if car_id field is valid
     */
    public function testValidCarId(): void
    {
        $this->assertTrue($this->validateField('car_id', 1));
        $this->assertFalse($this->validateField('car_id', null));
    }

    /**
     * Test if miles field is valid
     */
    public function testValidMiles(): void
    {
        $this->assertTrue($this->validateField('miles', 10.00));
        $this->assertFalse($this->validateField('miles', null));
    }

    /**
     * Check a field and value against validation rule
     *
     * @param string $field
     * @param mixed $value
     * @return bool
     */
    protected function validateField(string $field, $value): bool
    {
        return $this->validator->make(
            [$field => $value],
            [$field => $this->rules[$field]]
        )->passes();
    }
}
