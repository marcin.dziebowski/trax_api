<?php

namespace Http\Controllers\API;

use App\Car;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * Class CarControllerTest
 */
class CarControllerTest extends TestCase
{
    /**
     * Test if Car wil be created with given data return Response::201
     * @throws \Exception
     */
    public function testCanListsCars(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $cars = factory(Car::class, 5)->make();

        $user->cars()->saveMany($cars);

        $this->get(route('api_cars_index'))
            ->assertStatus(Response::HTTP_OK);

        $user->delete();
    }

    /**
     * Test if Car with given id will be show with Response::200
     * @throws \Exception
     */
    public function testCanShowCar(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->make();

        $user->cars()->save($car);

        $this->get(route('api_cars_show', $car->id))
            ->assertStatus(Response::HTTP_OK);

        $user->delete();
    }

    /**
     * Test if Car wil be created with given data return Response::201
     * @throws \Exception
     */
    public function testCanCreateCar(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $formData = [
            'make' => 'Ford',
            'model' => 'Mustang',
            'year' => 2022,
            'user_id' => $user->id
        ];

        $this->json('POST', route('api_cars_create'), $formData)
            ->assertStatus(Response::HTTP_CREATED);

        $user->delete();
    }

    /**
     * Test if Car wil be deleted with given id return Response::200
     * @throws \Exception
     */
    public function testCanDestroyCar(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->make();

        $user->cars()->save($car);

        $this->get(route('api_cars_delete', $car->id))
            ->assertStatus(Response::HTTP_OK);

        $user->delete();
    }
}
