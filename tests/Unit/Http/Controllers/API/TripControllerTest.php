<?php

namespace Http\Controllers\API;

use App\Car;
use App\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * Class CarControllerTest
 */
class TripControllerTest extends TestCase
{
    /**
     * Test if Car wil be created with given data return Response::201
     */
    public function testCanListsTrips(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $this->get(route('api_cars_index'))
            ->assertStatus(Response::HTTP_OK);
    }

    /**
     * Test if Trip wil be created with given data return Response::201
     * @throws \Exception
     */
    public function testCanCreateTrip(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user, 'api');

        $car = factory(Car::class)->create();
        $formData = [
            'date' => '06/10/2022',
            'miles' => 10.00,
            'car_id' => $car->id,
            'user_id' => $user->id
        ];

        $this->json('POST', route('api_trips_create'), $formData)
            ->assertStatus(Response::HTTP_CREATED);

        $car->delete();
        $user->delete();
    }
}
