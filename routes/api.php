<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

// endpoint to get all cars for the logged in user
Route::get('/cars', 'CarController@index')->middleware(['auth:api'])->name('api_cars_index');

// endpoint to add a new car.
Route::post('/cars', 'CarController@store')->middleware(['auth:api'])->name('api_cars_create');

// endpoint to get a car with the given id
Route::get('/cars/{id}', 'CarController@show')->middleware(['auth:api'])->name('api_cars_show');

// endpoint to delete a car with a given id
Route::delete('/cars/{id}', 'CarController@destroy')->middleware(['auth:api'])->name('api_cars_delete');

// endpoint to get the trips for the logged in user
Route::get('/trips', 'TripController@index')->middleware(['auth:api'])->name('api_trips_index');

// endpoint to add a new trip.
Route::post('/trips', 'TripController@store')->middleware(['auth:api'])->name('api_trips_create');
